module.exports = {
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/base',
    '@vue/eslint-config-airbnb',
  ],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
  },
};
