import { defineConfig } from 'vite';
import { createVuePlugin } from 'vite-plugin-vue2';
import vue from '@vitejs/plugin-vue2';

const path = require('path');

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: [
      {
        find: /^@\/(.+)/,
        replacement: `${path.resolve(path.resolve(__dirname), 'src')}/$1`,
      },
    ],
  },
});
